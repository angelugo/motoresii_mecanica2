﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{

    public Camera Cam;//Se declara una variable "Cam" de tipo Camera
    private Transform Target;//Se declara una variable "Traget" de tipo Transform para tener el Transform de un gameObject En espacial almacenado en una variable
    public GameObject Player1;//Se declara una variable "Player" de tipo GameObject y luego poder usar los metodos y propiedades de la clase GameObhject
    public GameObject Player2;//Se declara una variable "Player2" de tipo GameObject y luego poder usar los metodos y propiedades de la clase GameObhject
    public Vector3 OffSet;
    public float Valor;
    void Start()//Se invoca la funcion Start
    {
        Target = GameObject.Find("traget").transform;//Se iguala la variable target  al tranform de un objeto con nombre "target" em el proyecto, esto hace que nuestra variable tome el transform de el objeto con el nombre que indiquemos 
    }

    // Update is called once per frame
    void Update()//Se invoca la funcion Update
    {
        if (Player2 !=null)//Preguntamos si el player2 es distinto de null
        {
            if (Vector3.Distance(Player1.transform.position, Player2.transform.position) > 6)//Se invoca la sentencia if y se pregunta si la distasncia entre el gameObject "Player1" y "Player2" es superior a 6
            {
                if (Camera.main.orthographicSize <= 12)//Se usa la propiedad orthographicSize de Camera.main(Que hace referancia a la propiedad size de la camara en modo orthographic) es menor o igual a 12, esto para poder marcar un limite a la variable orthographicSize, si se llega a superar, entonces no se aplica
                {
                    Camera.main.orthographicSize += (Vector3.Distance(Player1.transform.position, Player2.transform.position) * Time.deltaTime);//Se igualaa a la suma la propiedad orthographicSize de Camera.main a la distancia entre Player1 y Player2, se usa time.DeltaTime para que el cambio no sea brusco 
                }
            }
            else//Si invoca a la sentencia else, que respode que en caso de no cumplirse la sencia if, entonces corre lo que definamos en su cuerpo
            {
                if (Camera.main.orthographicSize >= 7)//Se usa la propiedad orthographicSize de Camera.main(Que hace referancia a la propiedad size de la camara en modo orthographic) es mayor o igual a 7, esto para poder marcar un limite a la variable orthographicSize, si se llega a superar, entonces no se aplica
                {
                    Camera.main.orthographicSize -= (Vector3.Distance(Player1.transform.position, Player2.transform.position) * Time.deltaTime);//Se iguala ala resta la propiedad orthographicSize de Camera.main a la distancia entre Player1 y Player2, se usa time.DeltaTime para que el cambio no sea brusco 
                }
            }
        }
        if (Player2 == null)//Invocamos la sentencia if y pregntamos si Player2 es igual a null
        {
            if (Camera.main.orthographicSize>=7)//Si se cumple la condicion invocamos la setnecia if y prguntamos si la propiedad orthographicSize de la camara es mayor o igual a 7, con esto marcamos un limte de zoom
            {
                Camera.main.orthographicSize -= (7 * Time.deltaTime);//Igualamos el valor de la propiedad orthographicSize a la resta de 7 y lo multiplicamos por time.Deltatime para suavizar el movimiento
            }
        }
       
    }
    private void LateUpdate()
    {
      transform.position = Target.position;//Le asignamos  el valor de la position de la variable "target" a la trasform.position  que hace referecia a la posicion del gameObject que tiene este codigo 

      

        
        if (Input.GetKey(KeyCode.E))//Se invoca la sentencia if y se pregutna si se presiona la letra E  
        {
            Camera.main.orthographicSize = 7;// En caso de complircon la condicion entonces la propiedad orthographicSize se igual a 7
        }

    }
}