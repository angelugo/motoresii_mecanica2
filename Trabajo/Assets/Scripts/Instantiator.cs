﻿using System.Collections.Generic;
using UnityEngine;

public class Instantiator : MonoBehaviour
{
    public static List<Controller_Enemy> enemies;//Se genera una lista de tipo "Controller_Enemy" llamada enemies, esto para guarar un numero de enemigos en una lista
    public List<GameObject> enemy;//Se declara una lista de tipo "GameObject" llamada "enemy", esto para guarar un numero de enemigos en una lista
    public List<GameObject> positions;//Se declara una lista de tipo "GameObject" llamada "positions", esto para guarar las positions del donde queremos instanciar 
    public GameObject powerUp;//Declaramos una variable de tipo "GameObject" llamada "powerUp" 
    private GameObject powerUpInstance;//Declaramos una variable de tipo "GameObject" llamada "powerUpIstance" para guardar la instancia, esto nos sierve para que esa esta la variable que se destruya teniendo dentro alacenado el gameObject de "powerUp"  
    private float initialWaveDuration, initialAumentedWaveDuration, initialPowerUpTime;//Se declaran 3 variable de tipo floar llamadas "initialWaveDuration" que usaremos para guardar el valor inicial de la duracion que tendra la oleada, "initialAumentedWaveDuration" sera para guardar el valor inicial del aumento  de la duracion de la oleada
    public int wave = 1;//Se declara una variable  de tipo "Int" llamada "wave" y se le asigna el valor de 1
    public float waveDuration = 5, aumentedWaveDuration = 3, powerUpTime = 10;

    private void Start()//Declaramos la funcion Start
    {
        enemies = new List<Controller_Enemy>();//Se genera una nueva instnacia de la lista de tipo "Controller_Enemy" para que la variable "enemies" tenga la referencia
        initialWaveDuration = waveDuration;
        initialAumentedWaveDuration = aumentedWaveDuration;
        initialPowerUpTime = powerUpTime;
        Restart._Restart.OnRestart += Reset;
        SpawnEnemies();//Invocamos la funcion SpawnEnemies haciendo que se generem los enemigos 
    }

    private void Reset()//Se declara la funcion Reset que se llama para reestrablecr valores predeterminados 
    {
        waveDuration = initialWaveDuration;//Igualamos el WaveDuration al valor de initialWaveDuration para restablecer sus valores y que se quede en espera para la siguiente oleada
        aumentedWaveDuration = initialAumentedWaveDuration;//Igualamos el aumenteWaveDuration al valor de initialAumentedWaveDuration para restablecer sus valores y el aumento se reinicie 
        powerUpTime = initialPowerUpTime;//Igualamos el powerUpTime al valor de initialPowerUpTime para restablecer sus valores y que se reinicie el tempo para invocar el powerUp
        wave = 1;//Igualamos el valor de la variable "wave" a 1 y asi restrablem ell valor de la oleada 
        if (powerUpInstance != null)//Invacamos la sentencia if para preguntar si la variable powerUpInstance es distinto de null
            Destroy(powerUpInstance);//En caso de que se cumpla la condicion y powerUp no sea null entonces destruirmos el gameObject powerUp
        foreach (Controller_Enemy c in enemies)//Hacemos uso de un foreach para recorer "c" en la lista enemies
        {
            c.Reset();//Hacemos reset a los elemtos de la lista 
        }
        SpawnEnemies();//Por ultimo invicamos la funcion SpawnEnemies para realizar el ciclo de espera
    }

    private void Update()//Declaramos la funcion Update
    {
        waveDuration -= Time.deltaTime;//Le igualamos a la resta el valor de Time.deltaTime a "waveDuration"
        powerUpTime -= Time.deltaTime;//Le igualamos a la resta el valor de Time.deltaTime a "powerUpTime"
        if (powerUpTime < 0)//Invocamos la sentencia if y preguntamos que si powerUpTime es menor a 0
        {
            SpawnPowerUp();//Si se cumple nuestra condicion invocamos la funcion SpawnPowerUp y asi se invoca nuestro powerUp
        }
        if (waveDuration < 0)//Invocamos la sentencia if y preguntamos que si waveDuration es menor a 0
        {
            SpawnEnemies();//Si se cumple nuestra condicion invocamos la funcion SpawnPowerUp y asi se invocan nustros enemigos
        }
    }

    private void SpawnEnemies()//Declaramos  la funcion SpawnEnemies
    {
        //Toda esta funcion lo que hace es primero recorrer una cantidad de enemigos, luego se genera un valor random entre 0 y la cantiad de posiciones
        //Luego se instancia un enemigo random de la lista en una posicion random de la lista de posiciones 
        //Por ultimos se añade a la lista de enemigos el enemigo instanciado actualmente en el for
        //Luego restablecemos y asignamos valores 

        if (!Controller_Hud.gameOver)//Preguntamos si la variable gameOver de Controller_Hud es verdadero
        {
            int enemiesCount = wave * 2;
            for (int i = 0; i < enemiesCount; i++)
            {
                int random = UnityEngine.Random.Range(0, positions.Count);
                GameObject enemyInstance = Instantiate(enemy[UnityEngine.Random.Range(0, enemy.Count)], positions[random].transform.position, Quaternion.identity);
                enemies.Add(enemyInstance.GetComponent<Controller_Enemy>());
            }
            aumentedWaveDuration += 0.3f;
            waveDuration = aumentedWaveDuration;
            wave++;
        }
    }

    private void SpawnPowerUp()//Declaramos al funcion llamada SpawnPowerUp
    {
        //En esta funcion lo que hace primero generar una posicion random y guardarla en un vector
        //Luego se instancia  el powerUp en la posicion random
        //Y de ultimo se destruye la instancia del powerUp (No se Destruye el objeto ya que si no  nos tira error de referencia, por eso se hace una variable que guarde la instnacia )
        Vector3 randomizer = new Vector3(UnityEngine.Random.Range(-7, 7), 1, UnityEngine.Random.Range(-7, 7));
        powerUpInstance = Instantiate(powerUp, randomizer, Quaternion.identity);
        Destroy(powerUpInstance, 10);
        powerUpTime = 20;
    }

    private void OnDisable()
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
