﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player2 : MonoBehaviour
{
    public Camera cam;
    private Rigidbody rb;
    private Renderer render;
    public static Controller_Player2 _Player;
    private Vector3 movement;
    private Vector3 mousePos;
    internal Vector3 shootAngle;
    private Vector3 startPos;
    private bool started = false;
    public float speed = 5;
    public GameObject target;

    private void Start()
    {
        if (_Player == null)
        {
            _Player = this.gameObject.GetComponent<Controller_Player2>();
        }
        startPos = this.transform.position;
        rb = GetComponent<Rigidbody>();
        render = GetComponent<Renderer>();
        Restart._Restart.OnRestart += Reset;
        started = true;
        Controller_Shooting._ShootingPlayer.OnShooting += Shoot;
    }

    private void OnEnable()
    {
        if (started)
            Restart._Restart.OnRestart += Reset;
    }

    private void Reset()
    {
        this.transform.position = startPos;
    }

    private void Update()
    {
        movement = Vector3.zero;
        if (Input.GetKey(KeyCode.J))
        {
            movement.x = -1;
        }
        if (Input.GetKey(KeyCode.L))
        {
           movement.x = 1;
        }
        if (Input.GetKey(KeyCode.K))
        {
            movement.z = -1;
        }
        if (Input.GetKey(KeyCode.I))
        {
            movement.z = 1;
        }
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public virtual void FixedUpdate()
    {
       
        Movement(movement);
    }

    private void Movement(Vector3 direccion)
    {
        rb.MovePosition(rb.position + direccion.normalized * speed * Time.fixedDeltaTime);
     
        transform.LookAt(new Vector3(movement.x, 1, movement.z));
    }

    public Vector3 GetLastAngle()
    {
        if (Input.GetKey(KeyCode.I))
        {
            shootAngle = Vector3.forward;
        }
        if (Input.GetKey(KeyCode.H))
        {
            shootAngle = Vector3.left;
        }
        if (Input.GetKey(KeyCode.G))
        {
            shootAngle = Vector3.back;
        }
        if (Input.GetKey(KeyCode.J))
        {
            shootAngle = Vector3.right;
        }
        return shootAngle;
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("EnemyProjectile"))
        {
            Destroy(this.gameObject);
           // Controller_Hud.gameOver = true;
        }
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            int rnd = UnityEngine.Random.Range(1, 3);
            if (rnd == 1)
            {
                Shooting2.ammo = Ammo.Shotgun;
                Shooting2.ammunition = 5;
            }
            else if (rnd == 2)
            {
                Shooting2.ammo = Ammo.Cannon;
                Shooting2.ammunition = 5;
            }
            else
            {
                Shooting2.ammo = Ammo.Bumeran;
                Shooting2.ammunition = 1;
            }
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Bumeran2"))
        {
            Shooting2.ammo = Ammo.Bumeran;
            Shooting2.ammunition = 1;
            Destroy(collision.gameObject);
        }
    }
 
 

    void OnDisable()
    {
        Controller_Shooting._ShootingPlayer.OnShooting -= Shoot;
        Restart._Restart.OnRestart -= Reset;
    }

    private void Shoot()
    {
        if (Shooting2.ammo == Ammo.Cannon)
        {
            rb.AddForce(this.transform.forward * -4f, ForceMode.Impulse);
        }
    }
}
