﻿public class Anticipating : Controller_Enemy//Heredamos De la clase Controller_Enemy
{
    private void FixedUpdate()//Invovamos la funcion FixedUpdate
    {
        AnticipatingBehaviour();//Se invoca la funcion AnticipatingBehaviour
    }

    private void AnticipatingBehaviour()//Declaramos la funcion AnticipatingBehaviour
    {
        if (player != null)//Invocamos la sentencia if y preguntas si la variable player(GameObject) es distinto a Null
        {
            Controller_Player p = player.GetComponent<Controller_Player>();//Genrarmos una instancia(p) de el componenete Controller_Player 

            agent.SetDestination(player.transform.position + p.GetLastAngle() * 2);//Le damos la direccion al agente, y su direccion es el transfrom del player mas el ultimo angulo tomado por del disparo
            agent.SetDestination(player.transform.position);//Se le da la direccion a donde tiene que ir el agente
            if (player2 !=null)
            {
                if (EnRango1)//Invocamos La sentencia if para preguntar si "EnRango" es real
                {
                    agent.SetDestination(player2.transform.position);//Se le da la direccion al agente(Va al transform.position del player2)
                }
            }
           
        }
       
    }
}
