﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Controller_Enemy : MonoBehaviour
{
   public float Vida = 4;
    public Transform firePoint;
    public GameObject bulletPrefab;
    public float bulletForce;
    public float numeroDeEnemigos;
    public float NumeroDeEnemigosLimite;
    public bool SePuede;
    public static int numPatroler;
    internal GameObject player;//Declaramos una variable de tipo GameObject llamada "Player" y lo usamos para tener una referencia al jugaodr 1
    internal GameObject player2;//Declaramos una variable de tipo GameObject llamada "Player2" y lo usamos para tener una referencia al jugaodr 2
    internal NavMeshAgent agent;//Declaramos una variable de tipo NavMeshAgent llamada agente, para hacer referencia al enemigo que usa los componenetes de un agente en NavMesh
    internal Renderer render;//Declaramos una variable tipo Renderer llamada "render"
    internal Vector3 destination;//Declaramos una variable de tipo  Vector3 llamada destination 
    public float patrolDistance = 5;//Declaramos una variable publica de tipo float llamada "patrolDistance" y la inicializamos en un valor de 5, esta variable esta en funcion de guardad el valor de la distancia del enemigo Patrol que luego usamos en el codigo "Patrol"
    public float destinationTime = 4;//Declaramos un variable publica de tipo float llamada "destinationTime" y la inicializamos en 4, esta hace referencia al tiemmpo en el que este se detiene 
  
    public bool EnRango1;//Declaramos Una variable publica de tipo boolLlamada "EnRango1" y la usamos para para que tome un valor entre true y false 
    public bool EnRango2;//Declaramos Una variable publica de tipo boolLlamada "EnRango2" y la usamos para para que tome un valor entre true y false 

    void Start()//Invoca la funcion Start
    {
        render = GetComponent<Renderer>();//Igualamos la variable render a  GetComponent<Renderer> para referenciarlo con su tipo y poder usar sus componentes 
        Restart._Restart.OnRestart += Reset;//Aumentamos El valor de OnRestart
        destination = new Vector3(UnityEngine.Random.Range(-10, 12), 1, UnityEngine.Random.Range(-12, 9));//En esta liena lo qye hacemos es darle a la variable destination que es un vector, le pasamos valores random para los ejes X-Z
        agent = GetComponent<NavMeshAgent>();//Igualamos la variable agent a  GetComponent<NavMeshAgent> para referenciarlo con su tipo y poder usar sus componentes 
        player = GameObject.Find("Player");//Igualamos a la variable "player" a  GameObject.Find("Player" esto lo hacemos para referenciar un objeto con el nombre Player a nuestra Variable
        player2 = GameObject.FindGameObjectWithTag("Player2");//Igualamos a la variable "player" a GameObject.FindGameObjectWithTag("Player2") Para poder referenciar un objeto con la tag "Player2" a la variable "Player2" 

    }
    private void Update()//Invoca a funcion Update
    {
        if (player2 !=null)
        {
            if (Vector3.Distance(transform.position, player2.transform.position) < 5)//Invocamos la sentencia if y preguntamos si la distancia entre el GameObject que tien este codigo"Enemigo" y player 2 es menor que 5
            {
                EnRango1 = true;//Si se cumple la condicion entonces la variable "EnRango1" es true
            }
            if (Vector3.Distance(transform.position, player.transform.position) < 5)//Invocamos la sentencia if y preguntamos si la distancia entre el GameObject que tien este codigo"Enemigo" y player 1 es menor que 5
            {
                EnRango2 = true;//Si se cumple la condicion entonces la variable "EnRango2" es true
            }
        }
       
    }

    public void Reset()//Invoca la funcion Reset
    {
        Destroy(this.gameObject);//Destruye al objeto que tenga este codigo 
    }

     void OnCollisionEnter(Collision collision)//Invoca la funcion OnCollisionEnter para tomar la colisiones
    {
        if (collision.gameObject.CompareTag("Projectile"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Projectile"
        {
            Destroy(collision.gameObject);//Destruye el objeto con el que colisiono
            Destroy(this.gameObject);//Destrulle el gameObject que tiene este codigo 
            Controller_Hud.points++;//Aumenta los la propiedad points
        }
        if (collision.gameObject.CompareTag("CannonBall"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "CannonBall"
        {
            Destroy(this.gameObject);//Destrulle el gameObject que tiene este codigo 
            Controller_Hud.points++;//Aumenta los la propiedad points
        }
        if (collision.gameObject.CompareTag("Bumeran"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Bumeran"
        {
            Destroy(this.gameObject);//Destrulle el gameObject que tiene este codigo 
            Controller_Hud.points++;//Aumenta los la propiedad points
        }
        if (collision.gameObject.CompareTag("Bumeran2"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Bumeran2"
        {
            Destroy(this.gameObject);//Destrulle el gameObject que tiene este codigo 
            Controller_Hud.points++;//Aumenta los la propiedad points
        }
        if (collision.gameObject.CompareTag("Projectile2"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Bumeran2"
        {
            if (SePuede== false)
            {
                Destroy(collision.gameObject);//Destruye el objeto con el que colisiono

                StartCoroutine(Tiempo());

            }
        }
    }

    private void OnDestroy()//Invoca la funcion OnDestroy que se da cuando el objeto se destruye
    {
        Instantiator.enemies.Remove(this);//Remueve de la lisra el objeto que fue destruido 
    }

    private void OnDisable()//Se invoca la funcion OnDistable y se desactiva Un compotamiento
    {
        Restart._Restart.OnRestart -= Reset;//Decrementamos el valor de OnRestart
      
    
    }
    
    IEnumerator Tiempo()
    {
        SePuede = false;
        float Velocidad = agent.speed;
        agent.speed = 0;
        yield return new WaitForSeconds(3);
        agent.speed = Velocidad;
        SePuede = true;
    }
}
