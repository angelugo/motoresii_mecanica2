﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBoss : Controller_Enemy
{
    void Update()
    {
        FollowingBehaviourEnemy1();
    }
    private void FollowingBehaviourEnemy1()//Se declara la funcion FollowingBehaviour
    {
        if (player != null)//Se invoca la sentencia if y se pregunta si player(Gameobject) es distinto null
        {
            agent.SetDestination(player.transform.position);//Se le da un destino a agente (La posicion de la variable player)
            if (player2 != null)
            {
                if (EnRango1)//Se invoca la sentencia if y preguntamos si EnRango1 es true

                {
                    agent.SetDestination(player2.transform.position);//Si se cumple la condicion //Se le da un destino a agente (La posicion de la variable player2)
                }
            }

            if (EnRango2)//Se invoca la sentencia if y preguntamos si EnRango2 es true
            {
                agent.SetDestination(player.transform.position);//Se le da un destino a agente (La posicion de la variable player)
            }

        }
    
}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Projectile"
        {
            Vida -= 1;
            Destroy(collision.gameObject);//Destruye el objeto con el que colisiono
            transform.localScale = new Vector3(transform.localScale.x - 0.4f, transform.localScale.y - 0.4f, transform.localScale.z - 0.4f);

            if (Vida <= 0)
            {
                gameObject.SetActive(false);
                Controller_Hud.points++;//Aumenta los la propiedad points
            }
        }
        if (collision.gameObject.CompareTag("CannonBall"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Projectile"
        {
            Vida -= 2;
            Destroy(collision.gameObject);//Destruye el objeto con el que colisiono
            transform.localScale = new Vector3(transform.localScale.x - 0.6f, transform.localScale.y - 0.6f, transform.localScale.z - 0.6f);

            if (Vida <= 0)
            {
                gameObject.SetActive(false);
                Controller_Hud.points++;//Aumenta los la propiedad points
            }
        }
        if (collision.gameObject.CompareTag("Bumeran"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Projectile"
        {
            Vida -= 1;
            Destroy(collision.gameObject);//Destruye el objeto con el que colisiono
            transform.localScale = new Vector3(transform.localScale.x - 0.4f, transform.localScale.y - 0.4f, transform.localScale.z - 0.4f);

            if (Vida <= 0)
            {
                gameObject.SetActive(false);
                Controller_Hud.points++;//Aumenta los la propiedad points
            }
        }
        if (collision.gameObject.CompareTag("Bumeran2"))//Invica la sentencia if para preguntar si colisionamos con  un objeto con la tag "Projectile"
        {
            Vida -= 1;
            Destroy(collision.gameObject);//Destruye el objeto con el que colisiono
            transform.localScale = new Vector3(transform.localScale.x - 0.4f, transform.localScale.y - 0.4f, transform.localScale.z - 0.4f);

            if (Vida <= 0)
            {
                gameObject.SetActive(false);
                Controller_Hud.points++;//Aumenta los la propiedad points
            }
        }

    }
}
