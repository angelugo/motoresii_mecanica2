﻿using UnityEngine;

public class Patrol : Controller_Enemy//Heradamos De la clase "Controller_Enemy"
{
    private void FixedUpdate()//Declaramos la funcion FixedUpdate
    {
        Patroling();//Invocamos la funcion Patroling
    }

    private void Patroling()//Declaramos la funcion  Patroling
    {
   
        if (player != null)//Invocamos la sentencia if y preguntamos si player es distinto a nul
        {
            //En caso de cumplir con la condicion entonces se calcula la distancia entre el player y el gameObject que tenga este codigo y si esa distancia es menor que patrolDistance
            //Si es que se cumple la condicion entoces el agente va a al transform.position de player
            //En caso de no cumplir con la condicion se invoca a la funcion PatrolBehaviour

            if (Vector3.Distance(player.transform.position, transform.position) < patrolDistance)
            {
                agent.SetDestination(player.transform.position);
            }
            else
            {
                PatrolBehaviour();
            }
        }
    }

    private void PatrolBehaviour()//Declaramos una funcion llamada PatrolBehaviour
    {
        agent.SetDestination(destination);// Se le da una direccion al variable agent(NavMeshAngent) (el distino es igual a la variable destination)
        destinationTime -= Time.deltaTime;//Igualamos destinationTime a  la resta de Time.deltaTime
        if (destinationTime < 0)//Invocamos si destinationTime es menor a 0
        {
            destination = new Vector3(Random.Range(-10, 12), 1, Random.Range(-12, 9));//Igualamos la distancia a un vector3 donde el valor de X y de z son Ranfom
            destinationTime = 4;//Igualamos  a 4 destinationTime y asi vuelve a esperar 
        }
    }
}
