﻿using UnityEngine;

public class RandomEnemy : Controller_Enemy
{
    private void FixedUpdate()//Se declara la funcion FixedUpdate
    {
        PatrolBehaviour();//Se invoca la funcion PatrolBehaviour
    }

    private void PatrolBehaviour()//Se declara la funcion PatrolBehaviour
    {
        agent.SetDestination(destination);// Se le da una direccion al variable agent(NavMeshAngent) (el distino es igual a la variable destination)
        destinationTime -= Time.deltaTime;//Igualamos destinationTime a  la resta de Time.deltaTime
        if (destinationTime < 0)//Invocamos si destinationTime es menor a 0
        {
            destination = new Vector3(Random.Range(-10, 12), 1, Random.Range(-12, 9));//Igualamos la distancia a un vector3 donde el valor de X y de z son Ranfom
            destinationTime = 4;//Igualamos  a 4 destinationTime y asi vuelve a esperar 
        }
    }
}
