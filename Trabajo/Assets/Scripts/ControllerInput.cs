﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerInput : MonoBehaviour
{
    public GameObject Player2;
      public bool Espera;//Declaramos una variable de tipo bool, llamada  Espera
    private void Start()
    {
        Player2.SetActive(false);//Invocamos el metodo SetActive(false) para desactivar el gameObject(Player2)    
    }
    void Update()//Invocamos a la funcion Update
    {

        if (Input.GetKeyDown(KeyCode.U) && Espera == false)//Invocamos la sentencia if y preguntamos  si se detecta la letra 'U' y tambien termino el Espera
        {
            StartCoroutine(Reducir());//Incovamos la funcion StartCorutine y se invoca el la funcion Reducir 
        }
        if (Input.GetKeyDown("space"))//Invocamos la sentencia if y preguntamos  si se detecta la tecla "Space"
        {
            Player2.SetActive(true);//Si se cumple la condicion Activamos el gameObject
        }


    }
    IEnumerator Reducir()//Definimos un metodo IEnumerator
    {
        Espera = true;//Le asignamos el valor "true" a el timeSacle
        Time.timeScale = 0.2f;//Igualamos el valor de timeSacale a 0.4f
        yield return new WaitForSeconds(0.4f);//Usamos WaitForSeconds para esperar una cantiada de segundos(Se usa 0.4f ya que la escala del tiempo antes fue modificada y transcurre de forma distinta)
        Espera = false;//
        Time.timeScale = 1;//Le asignamos el valor "false" a el timeSacle


    }
}
