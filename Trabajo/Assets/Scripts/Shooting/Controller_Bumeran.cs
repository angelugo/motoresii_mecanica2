﻿using UnityEngine;

public class Controller_Bumeran : MonoBehaviour
{
    private Controller_Player parent;

    private Rigidbody rb;
    private CapsuleCollider collider;
    private Vector3 direction;
    public Vector3 startPos;
    public GameObject Player;
    public float maxDistance;
    public float bumeranSpeed;
    private float travelDistance;
    private float colliderTimer = 0.07f;
    private bool going;
    public float speed;

    void Start()
    {
        //En esta funcion inicializamos y referenciamos las variables, le damos el valor de true a going y hacer la referencia a player
        Player = GameObject.FindGameObjectWithTag("Player");
        parent = Controller_Player._Player;
      
        rb = GetComponent<Rigidbody>();
        Restart._Restart.OnRestart += Reset;
        collider = GetComponent<CapsuleCollider>();
        collider.enabled = false;
        going = true;
    }

    private void Reset()
    {
        Destroy(this.gameObject);
    }

    private void FixedUpdate()
    {
        //En esta funcion le damos la rotacion al gameObject llamando al funcion "Rotate" 
        //Tambien preguntamos si going es igual a true y en caso de hacerlo le indicamos que si la distancia de viaje es mayor a la distancia  maxima entonces invocamos la funcion CheckDirection
        //Esto para hacer que deje de avanzar 
        //Luego preguntamos en caso de no cumplir con la condicion  entonces invocamos la funcion ReturnToPlayer para que regrese al jugador 
        Rotate();
        if (going)
        {
            travelDistance = (startPos - transform.position).magnitude;
            if (travelDistance > maxDistance)
            {
                CheckDirection();
            }
        }
        else
        {
            ReturnToPlayer();
        }
    }

    void Update()
    {
        //En la funcion update lo que se hace es activar al bumeran y darle una direccion de viaje
        colliderTimer -= Time.deltaTime;
        if (colliderTimer < 0)
        {
            collider.enabled = true;
        }
        if (going)
        {
            travelDistance = (startPos - transform.position).magnitude;
        }
    }

    private void CheckDirection()
    {
        //En esta Funcion lo que hacemos es detener el viaje del bumeran, diciendo que going es falso que la velocidad es .zero
        going = false;
        rb.velocity = Vector3.zero;
        if (Controller_Player._Player != null)
        {
            direction = -(this.transform.localPosition - parent.transform.localPosition).normalized;
        }
    }

    private void Rotate()
    {
        //Esta funcion aplica la rotacion al objeto que tenga este codigo(El bumeran)
        transform.Rotate(new Vector3(10, 0, 0));
    }
      
    private void ReturnToPlayer()
    {
        //Esta funcion lo que hacemos es darle una direccion a donde ir al Bumeran
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, Player.transform.position,step);
    }

    private void OnDisable()//Se invoca la funcion OnDistable y se desactiva Un compotamiento
    {
        Restart._Restart.OnRestart -= Reset;//Decrementamos el valor de OnRestart
    }
}
