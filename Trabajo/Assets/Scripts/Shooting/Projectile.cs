﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    //Se declaran 2 variables que luego usaremos para hacer referencia al valor de los limites en X e Y
    public float xLimit = 30;
    public float yLimit = 20;

    private void Start()
    {
        //En la funcion Start lo que hacemos es hacer un reset 
        Restart._Restart.OnRestart += Reset;
    }

    private void Reset()
    {
        Destroy(this.gameObject);
    }

    virtual public void Update()
    {
        //En la funcion update Hacemos un check de si estamos superando los limites, para eso llamamos a la funcion CheckLimits
        CheckLimits();
    }

    internal virtual void OnCollisionEnter(Collision collision)
    {
        //En la funcion OnCollisionEnter pregutnamos si estamos colisionando con algun objeto con la tag "Wall" o con "powerUp, en caso de cumplirse la condicion estruimos el gameObject que tenga este codigo"
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("PowerUp"))
        {
            Destroy(this.gameObject);
        }
    }

    internal virtual void CheckLimits()
    {
        // En esta funcion lo que hacemos es ver si nuestra pocision super a la de los limites establecidos en las variables xLimt, que caso de superarlo entonces destruimos este gameObject
        if (this.transform.position.x > xLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.x < -xLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.z > yLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.z < -yLimit)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDisable()
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
