﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Controller_Shooting : MonoBehaviour
{
    public bool SePuede;
    public bool Activo;
    public delegate void Shooting();
    public event Shooting OnShooting;
    public static Ammo1 ammo;
    public static int ammunition;
    public static Controller_Shooting _ShootingPlayer;
    public Transform firePoint;
    public GameObject bulletPrefab;
    public GameObject RealentizadorPrefab;
    public GameObject cannonPrefab;
    public GameObject bumeranPrefab;
    public float bulletForce = 20f;
    private bool started = false;

    private void Awake()
    {
        if (_ShootingPlayer == null)
        {
            _ShootingPlayer = this.gameObject.GetComponent<Controller_Shooting>();
            Debug.Log("Shooting es nulo");
        }
        
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        if (_ShootingPlayer == null)
        {
            _ShootingPlayer = this.gameObject.GetComponent<Controller_Shooting>();
        }

        Restart._Restart.OnRestart += Reset;
        started = true;
        ammo = Ammo1.Bumeran;
        ammunition = 1;
    }

    private void OnEnable()
    {
        if (started)
            Restart._Restart.OnRestart += Reset;
    }

    private void Reset()
    {
        ammo = Ammo1.Bumeran;
        ammunition = 1;
    }

    void Update()
    {
        CambiarArmas();
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        

            CheckAmmo();
        }
    }

    private void CheckAmmo()
    {
        if (ammunition <= 0)
        {
            ammo = Ammo1.Normal;

        }
    }

    private void Shoot()
    {
        if (OnShooting != null)
        {
            OnShooting();
        }
        if (ammo == Ammo1.Normal)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
        }
        else if (ammo == Ammo1.Shotgun)
        {
            Rigidbody rb;
            for (float i = -0.2f; i < 0.2f; i += 0.1f)
            {
                rb = null;
                GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                rb = bullet.GetComponent<Rigidbody>();
                rb.AddForce(new Vector3(firePoint.forward.x + i, firePoint.forward.y, firePoint.forward.z + i) * bulletForce, ForceMode.Impulse);
            }
            ammunition--;
        }
        else if (ammo == Ammo1.Cannon)
        {
            GameObject bullet = Instantiate(cannonPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
            ammunition--;
        }
        else if (ammo == Ammo1.BugGun)
        {
            GameObject bullet = Instantiate(cannonPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
            
        }
        else if (ammo == Ammo1.Mt&&SePuede == false)
        {
            GameObject bullet = Instantiate(cannonPrefab, firePoint.position, firePoint.rotation);
                Rigidbody rb = bullet.GetComponent<Rigidbody>();

            bullet.transform.localScale = new Vector3(transform.localScale.x + 2, transform.localScale.y + 2, transform.localScale.z + 2);
            Destroy(bullet,3);
            StartCoroutine(TimepoEspera());



        }
        else if (ammo == Ammo1.Realentzador)
        {
            GameObject bullet = Instantiate(RealentizadorPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
        }

        else if (ammo == Ammo1.Bumeran)
        {
            GameObject bullet = Instantiate(bumeranPrefab, firePoint.position, firePoint.rotation);
            Controller_Bumeran bm = bullet.GetComponent<Controller_Bumeran>();
            bm.startPos = firePoint.position;
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
            ammunition--;
        }
    }

    private void OnDisable()
    {
        Restart._Restart.OnRestart -= Reset;
    }
    public void CambiarArmas()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ammo = Ammo1.Normal;
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ammo = Ammo1.BugGun;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ammo = Ammo1.Mt;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ammo = Ammo1.Realentzador;
        }

    }
    IEnumerator TimepoEspera()
    {
        SePuede = true;
        yield return new WaitForSeconds(3);
        SePuede = false;
    }


}

public enum Ammo1
{
    Normal,
    Shotgun,
    Cannon,
    Bumeran,
    BugGun,
    Realentzador,
    Mt
}
